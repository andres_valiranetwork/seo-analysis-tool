import { QuickSEOChecker } from "./Checkers";
import * as path from 'path';
import * as fs from 'fs';
import { BasicLighthouseChecker } from "./LighthouseChecker";
import { ILighthouseAnalysisResult, IWebsiteAnaylsisResult, IProgressFile, ITapTargetItem, ITestingUrlSuffix } from "./Interfaces";
import { SPANISH_URL, FRENCH_URL, ENGLISH_URL } from "./Urls";
/**
 * @description Path for the folder containing the movies websites.
 */
const checker:QuickSEOChecker = new QuickSEOChecker();

export class SEOAnalyzer {
    private static _instance:SEOAnalyzer;
    public progressFilePath:string;
    root_path = "/home/azolot/Documents/Important/_work/NamasteCorp/movies-websites/src/";
    conf_file_path = "/home/azolot/Documents/Important/_work/NamasteCorp/movies-webserver/src/config/websites/3001.example.website.js";

    public static get instance(){
        return this._instance ? this._instance : (()=>{
            this._instance = new SEOAnalyzer();
            return this._instance;
        })()
    }

    saveProgress(site:string, data:IWebsiteAnaylsisResult){
        try{
            let existing_object = this.getProgressFile() || {};
            //existing_object
            existing_object[site] = data;
            fs.writeFileSync(this.progressFilePath, JSON.stringify(existing_object));
            return true;
        }catch(e){
            console.log({e});
            return false;
        }
    }

    async analyzeWebsite(SITE:string, PAGES = [ '?debug=true&pass=sta9090' ], SITE_NAME="pelispedia.biz", resumeProgress=false){
        const ANALYSIS_URLS:string[] = []
        if(!SITE){
            SITE = "http://localhost:5000/es-ES";
        }
        for(const page of PAGES){
            ANALYSIS_URLS.push(`${SITE}/${page}`);
        }
        //console.log({ ANALYSIS_URLS });
        const _analysis_data:{[index:string]:{tags:string[], lighthouse:ILighthouseAnalysisResult}} = {};
        for(let i=0;i<ANALYSIS_URLS.length;i++){
            const url = ANALYSIS_URLS[i];
            try{
                console.log(`Running analysis for ${url}...`);
                let results = await Promise.all([checker.check(url), BasicLighthouseChecker.instance.check(url, true)]);
                console.log(`Finished analysis for ${url}`);
                //console.log({results:JSON.stringify(results)});
                _analysis_data[url] = _analysis_data[url] ? _analysis_data[url] : {
                    tags:[],
                    lighthouse: null
                };
                _analysis_data[url].tags = _analysis_data[url].tags.concat(results[0].data);
                _analysis_data[url].lighthouse = results[1];
                if(resumeProgress){
                    console.log("Saving progres...");
                    const saved = this.saveProgress(SITE_NAME, _analysis_data);
                    if(saved){
                        console.log("Progress saved.");
                    }else{
                        console.log("The progress hasn't been saved");
                    }
                }
            }catch(e){
                console.log(`Error analysing URL (${url}). Waiting default timeout before continuing...`);
                console.log({e});
                if((e+"").toLowerCase().indexOf('econnrefused')>-1){
                    console.log("Trying again due timeout...");
                    await this.serverReloadTimeout();
                    i--;
                }else{
                    console.log("Unhandled Error...");
                }
            }
        }
        return {data:_analysis_data, site:SITE};
    }

    getProgressFile(){
        if(fs.existsSync(this.progressFilePath)){
            return JSON.parse(fs.readFileSync(this.progressFilePath).toString());
        }
        return null;
    }

    hasStoredElement(progressFile:object, website_folder:string,  host:string, route:string){
        if(!host.endsWith('/')){
            host+="/";
        }
        return !!(progressFile && progressFile[website_folder] && progressFile[website_folder][`${host}${route}`]);
    }

    getLang(websiteHost = "http://localhost:5000/es-ES/"){
        websiteHost = websiteHost.endsWith('/') ? websiteHost : websiteHost+"/";
        return websiteHost.split("/")[websiteHost.split("/").length-2];
    }

    getPagesUrl(lang:string = 'es-ES'):ITestingUrlSuffix{
        if(lang.toLowerCase().indexOf('es')>-1) return SPANISH_URL;
        if(lang.toLowerCase().indexOf('fr')>-1) return FRENCH_URL;
        if(lang.toLowerCase().indexOf('en')>-1) return ENGLISH_URL;
        return ENGLISH_URL;
    }

    getPages(websiteHost = "http://localhost:5000/es-ES/", website_folder:string="cinecalidad.plus", resumeProgress:boolean){

        const website_path = path.resolve(path.join(this.root_path, website_folder));
        const pages_urls = [];

        if(fs.existsSync(website_path)){
            let progress_file = null;

            if(resumeProgress){
                progress_file = this.getProgressFile();
            }

            const lang_url = this.getPagesUrl(this.getLang(websiteHost));

            if(!this.hasStoredElement(progress_file, website_folder, websiteHost, '')){
                if(fs.existsSync(path.join(website_path, 'index.hbs'))){
                    pages_urls.push("");
                }
            }
            
            if(
                (!this.hasStoredElement(progress_file, website_folder, websiteHost, lang_url.search_found))
                || 
                (!this.hasStoredElement(progress_file, website_folder, websiteHost, lang_url.search_not_found))
            ){
                if(fs.existsSync(path.join(website_path, 'search.hbs'))){
                    if((!this.hasStoredElement(progress_file, website_folder, websiteHost, lang_url.search_found))){
                        pages_urls.push("busqueda/?s=Terminator&x=0&y=0");
                    }
    
                    if(!this.hasStoredElement(progress_file, website_folder, websiteHost, lang_url.search_not_found)){
                        pages_urls.push("busqueda/?s=aksjdkaskjdh&x=0&y=0");
                    }
                }
            }
            
            if(fs.existsSync(path.join(website_path, 'movies'))){
                const MOVIES_PATH = path.join(website_path, 'movies');
                if(!this.hasStoredElement(progress_file, website_folder, websiteHost, lang_url.category)){
                    if(fs.existsSync(path.join(MOVIES_PATH, 'category.hbs'))){
                        pages_urls.push(lang_url.category);
                    }
                }
                if(!this.hasStoredElement(progress_file, website_folder, websiteHost, lang_url.movie)){
                    if(fs.existsSync(path.join(MOVIES_PATH, 'movie.hbs'))){
                        pages_urls.push(lang_url.movie);
                    }
                }

                if(!this.hasStoredElement(progress_file, website_folder, websiteHost, lang_url.movies_now_playing)){
                    if(fs.existsSync(path.join(MOVIES_PATH, 'now_playing.hbs'))){
                        pages_urls.push(lang_url.movies_now_playing);
                    }
                }
                if(!this.hasStoredElement(progress_file, website_folder, websiteHost, lang_url.movies_top_rated)){
                    if(fs.existsSync(path.join(MOVIES_PATH, 'top_rated.hbs'))){
                        pages_urls.push(lang_url.movies_top_rated);
                    }
                }
                if(!this.hasStoredElement(progress_file, website_folder, websiteHost, lang_url.movies_upcoming)){
                    if(fs.existsSync(path.join(MOVIES_PATH, 'upcoming.hbs'))){
                        pages_urls.push(lang_url.movies_upcoming);
                    }
                }
            }
            if(fs.existsSync(path.join(website_path, 'series'))){
                const SERIES_PATH = path.join(website_path, 'series');
                if(!this.hasStoredElement(progress_file, website_folder, websiteHost, lang_url.series_airing_today)){
                    if(fs.existsSync(path.join(SERIES_PATH, 'airing_today.hbs'))){
                            pages_urls.push(lang_url.series_airing_today);    
                    }
                }

                if(!this.hasStoredElement(progress_file, website_folder, websiteHost, lang_url.series_on_air)){
                    if(fs.existsSync(path.join(SERIES_PATH, 'on_air.hbs'))){
                        pages_urls.push(lang_url.series_on_air);
                    }
                }
                
                if(!this.hasStoredElement(progress_file, website_folder, websiteHost, lang_url.series_popular)){
                    if(fs.existsSync(path.join(SERIES_PATH, 'popular.hbs'))){
                        pages_urls.push(lang_url.series_popular);    
                    }
                }
                if(!this.hasStoredElement(progress_file, website_folder, websiteHost, lang_url.serie_main)){
                    if(fs.existsSync(path.join(SERIES_PATH, 'serie.hbs'))){
                            pages_urls.push(lang_url.serie_main);    
                    }
                }
                if(!this.hasStoredElement(progress_file, website_folder, websiteHost, lang_url.series_season)){
                    if(fs.existsSync(path.join(SERIES_PATH, 'season.hbs'))){
                            pages_urls.push(lang_url.series_season);    
                    }
                }

                if(!this.hasStoredElement(progress_file, website_folder, websiteHost, lang_url.series_episode)){
                    if(fs.existsSync(path.join(SERIES_PATH, 'episode.hbs'))){
                        pages_urls.push(lang_url.series_episode);
                    }
                }
                if(!this.hasStoredElement(progress_file, website_folder, websiteHost, lang_url.series_top_rated)){
                    if(fs.existsSync(path.join(SERIES_PATH, 'top_rated.hbs'))){
                        pages_urls.push(lang_url.series_top_rated);
                    }
                }
            }
        }
        return pages_urls;
    }

    updateConfFile(site_folder:string){
        const content = fs.readFileSync(this.conf_file_path, {
            encoding:'UTF-8'
        }).toString();
        const reg = new RegExp(/(\/movies-websites\/src\/)([\w+._-]+)/g);
        const newContent = content.replace(reg, `/movies-websites/src/${site_folder}`);
        fs.writeFileSync(this.conf_file_path, newContent);
    }

    async serverReloadTimeout(additionalTime=3000){
        await new Promise((accept)=>{
            //Timeout of 3 seconds
            setTimeout(accept, additionalTime);
        });
    }

    getSitesFolders(ignore_folders=['common']){
        return fs.readdirSync(this.root_path).filter((current)=>{
            return ignore_folders.indexOf(current) < 0;
        });
    }

    async analizeSites(resumeProgress = true, host:string = "http://localhost:5000/es-ES/"){
        const site_folders = this.getSitesFolders();
        let sites = 0;
        let time = new Date().getTime();
        let result_data:{[index:string]:any} = {};
        for(let site_folder of site_folders){
            let pages = this.getPages(host, site_folder, resumeProgress);
            if(!pages){
                //If no pages to analyze
                continue;   //Continue to next website
            }
            this.updateConfFile(site_folder);
            await this.serverReloadTimeout(6000);
            console.log(`Analyzing ${site_folder}...`);
            try{
                const analysis_result = await this.analyzeWebsite(host, pages, site_folder, resumeProgress);
                result_data[site_folder] = analysis_result;
                sites++;
            }catch(err){
                console.log("Error running analysis on "+site_folder);
                console.log({err});
            }
        }
        time = Math.trunc(((new Date().getTime()-time)/1000));
        return {sites, time, result_data}
    }

    /**
     * @description Maps the raw data type to object.
     * @param data The raw data received
     */
    mapDataToProgressFile(data:any):IProgressFile{
        for(let site_id in data){
            data[site_id] = data[site_id].data;
        }
        return data;
    }

    printReport(result_data?:object, minAmmountIssues=1){
        if(!this.progressFilePath){
            throw new Error("Cannot print report: progress file path not specified");
        }
        if(!fs.existsSync(this.progressFilePath)){
            throw new Error(`Cannot print report: progress file doesn't exist at path: "${this.progressFilePath}"`);
        }
        let file:IProgressFile
        if(result_data){
            file = this.mapDataToProgressFile(result_data);
        }else{
            file = this.getProgressFile();
        }
        
        let _lines = [];
        for(let site_id in file){
            let _url_issues = [];
            let _site_issues = [];
            let _site_issues_qty = 0;
            for(let url in file[site_id]){
                _url_issues = [];
                
                if(file[site_id][url].tags){
                    for(let tag_issue of file[site_id][url].tags){
                        _url_issues.push(`TAG ANALYSIS -- ${tag_issue}`);
                    }
                }
                if(file[site_id][url].lighthouse){
                    if(file[site_id][url].lighthouse.desktop.seo_score < 1){
                        for(let issue_id in file[site_id][url].lighthouse.desktop.details){
                            _url_issues.push(
                                `LIGHTHOUSE ANALYSIS - desktop - ${issue_id} (${file[site_id][url].lighthouse.desktop.details[issue_id].text}): `
                                + (file[site_id][url].lighthouse.desktop.details[issue_id].items ? `Items to tackle: ${file[site_id][url].lighthouse.desktop.details[issue_id].items.length} ${
                                    issue_id === 'tap-targets' ? " -> "+file[site_id][url]
                                    .lighthouse.desktop.details[issue_id]
                                    .items.map((element:ITapTargetItem)=>{
                                        return `[ ${element.overlappingTarget.selector} (--overlaps-->) ${element.tapTarget.selector} ]`
                                    }).join(", ") : ''
                                }` : file[site_id][url].lighthouse.desktop.details[issue_id].text)
                            );
                        }
                    }
                    if(file[site_id][url].lighthouse.mobile.seo_score < 1){
                        for(let issue_id in file[site_id][url].lighthouse.mobile.details){
                            _url_issues.push(
                                `LIGHTHOUSE ANALYSIS - mobile - ${issue_id} (${file[site_id][url].lighthouse.mobile.details[issue_id].text}): `
                                + (file[site_id][url].lighthouse.mobile.details[issue_id].items ? `Items to tackle: ${file[site_id][url].lighthouse.mobile.details[issue_id].items.length} ${
                                    issue_id === 'tap-targets' ? " -> "+file[site_id][url]
                                    .lighthouse.mobile.details[issue_id]
                                    .items.map((element:ITapTargetItem)=>{
                                        return `[ ${element.overlappingTarget.selector} (--overlaps-->) ${element.tapTarget.selector} ]`
                                    }).join(", ") : ''
                                }` : file[site_id][url].lighthouse.mobile.details[issue_id].text)
                            );
                        }
                    }
                }
                if(_url_issues.length && _url_issues.length >=  minAmmountIssues){
                    _site_issues.push(
                        `Found ${_url_issues.length} different issues on ${url}:`
                        +"\n\t\t"+_url_issues.join("\n\t\t")
                    );
                    _site_issues_qty+=_url_issues.length;
                }
            }
            if(_site_issues_qty){
                _lines.push(`Found ${_site_issues_qty} issues on ${site_id}:`);
                _lines.push(_site_issues.join("\n"));
            }else{
                _lines.push(`Found NO issues on ${site_id}`);
            }
        }
        console.log(_lines.join('\n'));
    }
}
