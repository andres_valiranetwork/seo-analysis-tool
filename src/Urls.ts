import { ITestingUrlSuffix } from "./Interfaces";

export const SPANISH_URL:ITestingUrlSuffix = {
    search_found: 'busqueda/?s=Terminator&x=0&y=0',
    search_not_found: 'busqueda/?s=aksjdkaskjdh&x=0&y=0',
    category: 'categoria/accion/',
    movie: 'castellano/ver-online/2020/sonic-la-pelicula/',
    movies_now_playing: 'movies/now-playing/',
    movies_top_rated: 'movies/top-rated/',
    movies_upcoming: 'movies/upcoming/',
    series_airing_today: 'series/airing-today',
    series_on_air: 'series/on-air',
    series_popular: 'series/popular',
    series_season: 'castellano/ver-online/the-walking-dead/s0/e1/',
    series_episode: 'series/castellano/ver-online/the-walking-dead/s0/',
    serie_main: 'series/castellano/ver-online/the-walking-dead/',
    series_top_rated: 'series/top-rated'
}

export const FRENCH_URL:ITestingUrlSuffix = {
    search_found: 'busqueda/?s=Terminator&x=0&y=0',
    search_not_found: 'busqueda/?s=aksjdkaskjdh&x=0&y=0',
    category: 'categoria/action/',
    movie: 'latino/pelicula-completa/2020/birds-of-prey-et-la-fantabuleuse-histoire-de-harley-quinn/',
    movies_now_playing: 'movies/now-playing/',
    movies_top_rated: 'movies/top-rated/',
    movies_upcoming: 'movies/upcoming/',
    series_airing_today: 'series/airing-today',
    series_on_air: 'series/on-air',
    series_popular: 'series/popular',
    series_season: 'series/latino/serie-completa/the-walking-dead/s0/e1/',
    series_episode: 'series/latino/serie-completa/the-walking-dead/s0/',
    serie_main: 'series/latino/serie-completa/the-walking-dead/',
    series_top_rated: 'series/top-rated'
}

export const ENGLISH_URL:ITestingUrlSuffix = {
    search_found: 'busqueda/?s=Terminator&x=0&y=0',
    search_not_found: 'busqueda/?s=aksjdkaskjdh&x=0&y=0',
    category: 'categoria/action/',
    movie: 'latino/pelicula-completa/2020/sonic-the-hedgehog/',
    movies_now_playing: 'movies/now-playing/',
    movies_top_rated: 'movies/top-rated/',
    movies_upcoming: 'movies/upcoming/',
    series_airing_today: 'series/airing-today',
    series_on_air: 'series/on-air',
    series_popular: 'series/popular',
    series_season: 'series/latino/serie-completa/the-flemish-bandits/s1/e1/',
    series_episode: 'series/latino/serie-completa/the-flemish-bandits/s1',
    serie_main: 'series/latino/serie-completa/the-flemish-bandits',
    series_top_rated: 'series/top-rated'
}