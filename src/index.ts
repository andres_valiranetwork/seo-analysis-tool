/*
CONFLICT (content): Merge conflict in src/movie4k.movie/static/css/custom.css
CONFLICT (content): Merge conflict in src/movie4k.movie/movies/movie.hbs
CONFLICT (content): Merge conflict in src/movie4k.movie/index.hbs
CONFLICT (content): Merge conflict in src/frenchstream.fr/static/css/custom.css
*/
import { SEOAnalyzer } from "./Analyzer";
import * as path from 'path';
import * as fs from 'fs';

SEOAnalyzer.instance.progressFilePath = path.resolve('./results/progress_file.json');


/*
SEOAnalyzer.instance.getPages = function(...params){
    return [
        'series/latino/serie-completa/the-flemish-bandits',
        'series/latino/serie-completa/the-flemish-bandits/s1/e1',
        'series/latino/serie-completa/the-flemish-bandits/s1',
    ];
}
//*/

const ENGLISH_SITES = [
    'cinecalidad.plus',
    'clivertv.org',
    'dpstream.movie',
    'filmstarts.film',
    'frenchstream.fr',
    'gomovies.theater',
    'miradetodo.online',
    'movie4k.movie',
    'movies123.biz',
    'pelis24.rip',
    'pelispedia.plus',
    'pelisplay.biz',
    'pelisplay.top',
    'pelisplay.plus',
    'repelis.biz',
    'streamcomplete.film',
    'topflix.film',
    'voirfilms.lu',
    'watchfull.movie',
    'wikiserie.biz'
];

const SPANISH_SITES = [
    'cuevana2.online',
    'elitetorrent.movie',
    'inkapelis.pro',
    'somosmovies.org'
];

SEOAnalyzer.instance.getSitesFolders = function(){
    return ['wikiserie.biz'];
};

const lang_hashes = {
    'en':'en-US',
    'es':'es-ES',
    'fr':'fr-FR',
    'it':'it-IT',
    'pt':'pt-PT'
}

function getLanguage(hash?){
    if(hash){
        return lang_hashes[hash];
    }
    const folder = SEOAnalyzer.instance.getSitesFolders();
    const files = fs.readdirSync(path.join(SEOAnalyzer.instance.root_path, folder[0], 'translations'));
    let lang = lang_hashes[files[0].split('.')[0]];
    if(!lang){
        console.log("Language not found...");
    }
    return lang || 'en-US';
}

const use_progress = false;

let host_url = `http://localhost:5000/${getLanguage('en')}`;

///*
SEOAnalyzer.instance.analizeSites(use_progress, host_url)
.then(({sites, time, result_data})=>{
    console.log(`Analized ${sites} in ${time} seconds.`);
    SEOAnalyzer.instance.printReport(use_progress ? null : result_data);
})
.catch((err)=>{
    console.log({err});
});
//*/

//SEOAnalyzer.instance.printReport(null, 3);