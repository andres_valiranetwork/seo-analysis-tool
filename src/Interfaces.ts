export interface ILighthouseAnalysisResult {
    desktop: {
        details: any;
        seo_score: number;
    };
    mobile: {
        details: any;
        seo_score: number;
    };
}

export enum IItemType {
    text, code, url, node
}

export enum IScoreDisplayMode {
    notApplicable, binary, manual, numeric, informative
}

export interface ITapTargetItem {
    tapTarget: {
        type: IItemType,
        snippet: string
        path: string
        selector: string
        nodeLabel: string
    },
    overlappingTarget: {
        type: IItemType,
        snippet: string,
        path: string,
        selector: string,
        nodeLabel: string
    },
    tapTargetScore: number,
    overlappingTargetScore: number,
    overlapScoreRatio: 1,
    size: string,
    width: 436,
    height: 11
}

export interface IFontSizeItem {
    source: string;
    selector: string;
    coverage: string;
    fontSize: string;
}

export interface IAuditResult {
    id: string;
    title: string;
    description: string;
    score: number;
    scoreDisplayMode: IScoreDisplayMode;
    warnings?: any[];
    explanation?: string;
    text: string;   //Either explanation or text
    details?: {
        type: string,
        headings: any[],
        items: any[]
    }
}

export interface IWebsiteAnaylsisResult {
    [index: string]: {
        tags: string[];
        lighthouse: ILighthouseAnalysisResult;
    }
}

export interface IPFLighthouseItem {
    id:string,
    text:string,
    items:any[]
}

export interface IPFLighthouseDeviceResult {
    details:{
        [index:string]:IPFLighthouseItem    //Each item has an ID as key
    },
    seo_score:number
}

export interface IProgressFile {
    [index:string]:{        //Site folder / name
        [index:string]: {   //Site URL analyzed
            tags:string[],  //The tag analysis checklist
            lighthouse:{
                desktop:IPFLighthouseDeviceResult,  //The desktop analysis results
                mobile: IPFLighthouseDeviceResult   //The mobile analysis result
            }
        }
    }
}

export interface ITestingUrlSuffix {
    search_found: string,
    search_not_found: string,
    category: string,
    movie: string,
    movies_now_playing: string,
    movies_top_rated: string,
    movies_upcoming: string,
    series_episode: string,
    series_on_air: string,
    series_airing_today: string,
    series_popular: string,
    series_season: string,
    serie_main: string,
    series_top_rated: string
}