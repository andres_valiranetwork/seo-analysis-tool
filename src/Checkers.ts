import * as fs from 'fs';
import * as path from 'path';
import { SEOChecker } from "./Checker";
const {Configure, Rule, Reader} = require('check-seo');
import fetch from 'node-fetch';
const {
    defaultRules,
    ruleBuilders,
    getDom,
    seoQuickChecker,
    output,
} = require('seo-quick-checker');

class SEOCheckerArrayWriter {
    public results_array: string[];

    constructor(){
        this.results_array = [];
    }

    write(output:string){
        const _newItems = output.split("\n") ? output.replace(/\r/g,'').split("\n").map((element)=>element.trim()) : [output];
        this.results_array = _newItems;
    }
}

/**
 *  Tags to check: 
 *  * Meta:
 *      **Refs:
 *          * https://moz.com/blog/the-ultimate-guide-to-seo-meta-tags
 *  ** Meta content type
 *  ** Title
 *  ** Meta description
 *  ** Viewport
 *  ** The canonical tag (rel="canonical" in header)
 *  
 *  * Body:
 *  ** Title tag
 *  ** Header Tags (h1: only one)
 *  ** Image alt. tags (All images should posses the alt attribute and have a text for it)
 */
export class BasicSEOChecker extends SEOChecker {
    private static _instance: BasicSEOChecker;

    private _SEOCheckerRules = [
        //Meta tags

        //Has the content-type meta tag
        new Rule.RuleExistsTagAttribute('head', 'meta', 'http-equiv', 'content-type'),
        //Has the title tag
        new Rule.RuleExistsTag('head', 'title'),
        //Has the name description
        new Rule.RuleExistsTagAttribute('head', 'meta', 'name', 'description'),
        //Has the viewport meta
        new Rule.RuleExistsTagAttribute('head', 'meta', 'name', 'viewport'),
        //Has the canonical tag in header
        new Rule.RuleExistsTagAttribute('head', 'link', 'rel', 'canonical'),
        // Body tags
        //Has a <H1> title tag
        new Rule.RuleExistsTag('body', 'h1'),
        //Has only 1 <H1> max title tag
        //new Rule.RuleMaxTagAttribute('body', 'h1'),
        //All image tags have an alt. text
        new Rule.RuleAllContainTagAttribute('', 'img', 'alt')
    ];

    static get instance(){
        return this._instance ? this._instance : (()=>{
            this._instance = new BasicSEOChecker();
            return this._instance;
        })()
    }

    async check(url:string){
        //stream: NodeJS.ReadableStream, rules=this.rules
        const readable = await this.getWebsiteReadableStream(url);
        const _res = await this.anaylizeWithSEOChecker(<any>readable, this._SEOCheckerRules);
        return _res;
    }

    async anaylizeWithSEOChecker(stream: NodeJS.ReadableStream, rules=this._SEOCheckerRules){
        let {error, data} = await new Promise((accept)=>{
            let _writer = new SEOCheckerArrayWriter();
            let conf = new Configure(new Reader.readStream(stream), _writer, rules);
            conf.validate((error)=>{
                if(error){
                    throw error;
                }
                accept({error, data:_writer.results_array});
            });
        });
        return {error, data};
    }

    get randomId(){
        return Math.floor(Math.random()*Number.MAX_SAFE_INTEGER).toString(16);
    }

    createPageFile(pageHtml:string):string {
        const tempPath = path.resolve(`./_temp/_temp_${this.randomId}.html`);
        fs.writeFileSync(tempPath, pageHtml, {
            encoding: 'utf-8'
        });
        return tempPath;
    }

    async getWebsiteReadableStream(url:string):Promise<NodeJS.ReadableStream>{
        const body:NodeJS.ReadableStream = await new Promise((accept, reject)=>{
            fetch(url)
            .then(res => accept(res.body))
            .catch((err)=>{
                reject(err);
            })
        });
        return body;
    }

    async getWebsiteHTML(url:string):Promise<string>{
        const text = await fetch(url)
        .then(res => res.text());
        return text;
    }
}


export class QuickSEOChecker extends SEOChecker {
    private static _instance: QuickSEOChecker;

    private _SEOCheckerRules:any[] = [
        ruleBuilders.tagExists("h1"),
        ruleBuilders.tagCountMoreThan('h1', 1),
        ruleBuilders.tagMissingAttr('img', 'alt'),
        ruleBuilders.tagExists('head meta[http-equiv=content-type]'),
        ruleBuilders.tagExists('head meta[name=description]'),
        ruleBuilders.tagExists('head meta[name=viewport]'),
        ruleBuilders.tagExists('head meta[rel=canonical]')
    ];

    static get instance(){
        return this._instance ? this._instance : (()=>{
            this._instance = new QuickSEOChecker();
            return this._instance;
        })()
    }

    async check(url:string){
        //stream: NodeJS.ReadableStream, rules=this.rules
        const readable = await this.getWebsiteReadableStream(url);
        const _res = await this.anaylizeWithSEOChecker(<any>readable, this._SEOCheckerRules);
        return _res;
    }

    async anaylizeWithSEOChecker(stream: NodeJS.ReadableStream, rules=this._SEOCheckerRules){
        const dom = await getDom.fromStream(stream)
        let {error, data} = await new Promise((accept)=>{
            let data = seoQuickChecker(dom, rules);
            accept({error, data});
        });
        return {error, data};
    }

    get randomId(){
        return Math.floor(Math.random()*Number.MAX_SAFE_INTEGER).toString(16);
    }

    createPageFile(pageHtml:string):string {
        const tempPath = path.resolve(`./_temp/_temp_${this.randomId}.html`);
        fs.writeFileSync(tempPath, pageHtml, {
            encoding: 'utf-8'
        });
        return tempPath;
    }

    async getWebsiteReadableStream(url:string):Promise<NodeJS.ReadableStream>{
        const body:NodeJS.ReadableStream = await new Promise((accept, reject)=>{
            fetch(url)
            .then(res => accept(res.body))
            .catch((err)=>{
                reject(err);
            })
        });
        return body;
    }

    async getWebsiteHTML(url:string):Promise<string>{
        const text = await fetch(url)
        .then(res => res.text());
        return text;
    }
}
