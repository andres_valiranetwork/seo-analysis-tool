import * as lighthouse from 'lighthouse';
import { launch, LaunchedChrome } from 'chrome-launcher';
import { SEOChecker } from './Checker';
import { IAuditResult, ILighthouseAnalysisResult, IScoreDisplayMode, ITapTargetItem } from './Interfaces';


export class BasicLighthouseChecker extends SEOChecker {
    private static _instance: BasicLighthouseChecker;
    static get instance() {
        return this._instance ? this._instance : (() => {
            this._instance = new BasicLighthouseChecker();
            return this._instance;
        })()
    }

    desktopChromeInstance: LaunchedChrome;

    constructor() {
        super();
    }
    /**
     * 
     * Audits that gets special treatments for parsing:
     * -- tap-targets
     * -- 
     */

    /**
     * @description Gets the audits with problems.
     */
    filterRelevantAudits(audits:{[index:string]:IAuditResult}){
        const _filteredAudits:{[index:string]:IAuditResult} = {};
        for(let key in audits){
            if(audits[key].scoreDisplayMode !== IScoreDisplayMode.notApplicable && audits[key].score !== null){
                if(audits[key].score < 1){
                    _filteredAudits[key] = audits[key];
                }
            }
        }
        return _filteredAudits;
    }

    mapRelevantAudits(audits:{[index:string]:IAuditResult}){

        for(let key in audits){
            if(audits[key].id === 'tap-targets'){
                (<any>audits[key]) = {
                    id: audits[key].id,
                    text: audits[key].explanation ? audits[key].explanation : audits[key].title,
                    items: audits[key].details ? audits[key].details.items.map((item:ITapTargetItem)=>{
                        return {
                            tapTarget: {
                                //snippet: item.tapTarget.snippet,
                                selector: item.tapTarget.selector
                            },
                            overlappingTarget: {
                                //snippet: item.overlappingTarget.snippet,
                                selector: item.overlappingTarget.selector
                            }
                        }
                    }) : []
                }
            }else{
                (<any>audits[key]) = {
                    id: audits[key].id,
                    text: audits[key].explanation ? audits[key].explanation : audits[key].title,
                    items: audits[key].details ? (audits[key].details.items ? audits[key].details.items.map((element)=>{
                        for(let key in element){
                            element[key] = {
                                text: element[key].explanation ? element[key].explanation : element[key].title,
                                selector: element[key].selector
                            }
                        }
                        return element;
                    }) : audits[key].details) : []
                }
            }
        }
        return audits;
    }

    /**
     * @description Analizes an URL and gets the related audit information
     * @param url URL of the page
     * @param reuseChromeInstance Wether we'll keep this chrome instance open or not
     */
    async getSEOData(url:string, reuseChromeInstance = false):Promise<ILighthouseAnalysisResult>{
        //url, opts, config = null
        if(!this.desktopChromeInstance){
            if(!this.desktopChromeInstance){
                this.desktopChromeInstance = await launch({ chromeFlags: ['--headless', '--disable-gpu'] });//
            }
        }
        //this.desktopChromeInstance = await launch();
        const SEO_audits = [
            "viewport",
            "document-title",
            "meta-description",
            "heading-order",
            "http-status-code",
            "link-text",
            "is-crawlable",
            "robots-txt",
            "image-alt",
            "hreflang",
            "canonical",
            "font-size",
            "plugins",
            "tap-targets",
            "structured-data",
            "seo"
        ];
        const desktop_options = {
            port: this.desktopChromeInstance.port
            , onlyAudits: SEO_audits
            ,emulatedFormFactor: 'desktop'
        };
        const mobile_options = {
            port: this.desktopChromeInstance.port
            , onlyAudits: SEO_audits
            ,emulatedFormFactor: 'mobile'
        };

        const res_desktop = (await lighthouse(url, desktop_options)).lhr;
        const res_mobile = (await lighthouse(url, mobile_options)).lhr;

        const result = {
            desktop: {details: this.mapRelevantAudits(this.filterRelevantAudits(res_desktop.audits)), seo_score: res_desktop.categories.seo.score},
            mobile: {details: this.mapRelevantAudits(this.filterRelevantAudits(res_mobile.audits)), seo_score: res_mobile.categories.seo.score}
        };
        if(!reuseChromeInstance){
            if(this.desktopChromeInstance){
                await this.desktopChromeInstance.kill();
                this.desktopChromeInstance = null;
            }
        }
        return result;
    }

    async check(url = "http://localhost:5000/es-ES/", reuseChromeInstance = false) {
        const data = await this.getSEOData(url, reuseChromeInstance);
        return data;
    }
}